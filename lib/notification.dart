import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notificationapp/login.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:loading/loading.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'constants.dart' as Constants;
import 'graphql_config.dart';
import 'package:loadmore/loadmore.dart';
import 'package:incrementally_loading_listview/incrementally_loading_listview.dart';


void onEnterPWA() async {
  const url = 'https://vuejs.org/v2/guide/';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}


List<TextSpan> formatText(String text) {
  var data = text.split("<b>");
  List<TextSpan> listText = [];

  for (var i = 0; i < data.length; i++) {
    if (data[i].contains("</b>")) {
      var listWord = data[i].split("</b>");
      listText.add(TextSpan(
          text: listWord[0],
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 12,
              color: Colors.black87)));
      listText.add(TextSpan(
          text: listWord[1],
          style: TextStyle(fontSize: 12, color: Colors.black87)));
    } else {
      listText.add(TextSpan(
          text: data[i],
          style: TextStyle(fontSize: 12, color: Colors.black87)));
    }
  }
  return listText;
}

String getDuration(String date) {
  var created = DateTime.parse(date);
  var duration = DateTime.now().difference(created).inSeconds;
  if (duration / 604800 >= 1)
    return (duration / 604800).toInt().toString() + " week";
  else if (duration / 86400 >= 1)
    return (duration / 86400).toInt().toString() + " day";
  else if (duration / 3600 >= 1)
    return (duration / 3600).toInt().toString() + " hour";
  else if (duration / 60 >= 1)
    return (duration / 60).toInt().toString() + " minute";
  else
    return duration.toString() + " second";

  return duration.toString();
}

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationState createState() => _NotificationState();
  final user;
  NotificationScreen({Key key, @required this.user});
}

class _NotificationState extends State<NotificationScreen> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  List listNotification = [];
  bool isLoading = false;
  int page = 1;
  bool pageEnd = false;
  GraphQLConfiguration graphQLConfiguration;
  ValueNotifier<GraphQLClient> client;
  GraphQLClient clientToQuery;
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    //graphQLConfiguration = GraphQLConfiguration(widget.user);

    //fireBaseCloudMessagingListeners();

    final HttpLink httpLink = HttpLink(
      uri: '${Constants.API_URL}/graphql',
    );
    print(widget.user["access_token"]);
    final AuthLink authLink = AuthLink(
      getToken: () => 'Bearer ${widget.user["access_token"]}',
    );
    final Link link = authLink.concat(httpLink);
    client = ValueNotifier(
      GraphQLClient(
        cache: InMemoryCache(),
        link: link,
      ),
    );


    clientToQuery = GraphQLClient(
      cache: InMemoryCache(),
      link: link,
      //defaultPolicies:DefaultOptions()
    );
    onLoad();
  }

  Future<bool> onLoad() async {

    QueryResult result = await clientToQuery.query(
      QueryOptions(documentNode: gql(queryString), variables: {"page": page},
        fetchPolicy: FetchPolicy.cacheAndNetwork,
      ),

    );
    if (!result.hasException) {
      print(result.exception);
      //onSignOut();
      if (result.data["notifications"]["data"].length>0) {
        print(result.data);
        setState(() {
         listNotification = (result.data["notifications"]["data"]);
          //listNotification =[];
          isLoading = false;
        });

      }else{
        setState(() {
          pageEnd = true;
        });
      }
    }else{
      onSignOut();
    }
    setState(() {
      isLoading = false;
    });
    return true;
  }

  nextPage() async {
    if (!pageEnd) {
      page++;
      print("page load: ${page}");
      setState(() {
        isLoading = true;
      });
      QueryResult result = await clientToQuery.query(
        QueryOptions(documentNode: gql(queryString), variables: {"page": page},
          fetchPolicy: FetchPolicy.cacheAndNetwork,
        ),
      );

      if (!result.hasException) {
        print("length data: ${result.data["notifications"]["data"].length}");
        if (result.data["notifications"]["data"].length > 0) {
          setState(() {
            listNotification.addAll(result.data["notifications"]["data"]);
          });
          setState(() {
            isLoading = false;
          });
        } else {
          setState(() {
            pageEnd = true;
          });
        }
      }else{
        setState(() {
          isLoading = false;
        });
      }
    }
    return false;
  }

  Future<bool> removeToken(id) async {
    QueryResult mutation = await clientToQuery.mutate(
        MutationOptions(document: mutationQuery, variables: {"id": id}));
    print(mutation.exception);
    if (mutation.hasException) return false;

    return true;
  }

  void onSignOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    removeToken(widget.user["app_token_id"]).then((value) {
      if (value) {
        prefs.remove("longin_info").then((value) {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Login()));
        });
      }
    });
  }

  String queryString = r"""
  	query Notification($page:Int!){
	  notifications(first: 10, page:$page) {
	    data {
	      id
	      notifiable {
	      	email
	        full_name
	        nationality,
	        avatar
	      }
	      data {
	        title
	        description,
	        
	      },
	      created_at,
	    }
	  }
	}
  """;

  String mutationQuery = r"""
  	mutation DeleteUserToken($id: ID!){
	  deleteUserToken(id:$id)
	  {id}
	}
  """;

  Future<bool> onRefresh() async{
    print("reload");
    setState(() {
      listNotification = [];
      pageEnd = false;
      page = 1;
    });
    return onLoad().then((value) => true);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: GraphQLProvider(
        client: client,
        child: Scaffold(
            appBar: AppBar(
              title: Text("Notification"),
            ),
            drawer: Drawer(
              // Add a ListView to the drawer. This ensures the user can scroll
              // through the options in the drawer if there isn't enough vertical
              // space to fit everything.

              child: ListView(
                // Important: Remove any padding from the ListView.
                padding: EdgeInsets.zero,
                children: <Widget>[
              DrawerHeader(
                padding: EdgeInsets.zero,
              child: UserAccountsDrawerHeader(
                accountName: Text(widget.user["user"]["full_name"]),
                  accountEmail: Text(widget.user["user"]["email"]),
                currentAccountPicture: CircleAvatar(
                  
                  backgroundColor:
                  Theme.of(context).platform == TargetPlatform.iOS
                      ? Colors.blue
                      : Colors.white,
                 // backgroundImage: NetworkImage("${Constants.API_URL}/storage/${widget.user["user"]["avatar"]}"),
                ),
              ),

              ),
                  ListTile(
                    leading: Icon(
                      Icons.exit_to_app,
                      color: Colors.red,
                    ),
                    title: Text('Sign Out'),
                    onTap: () {
                      // Update the state of the app
                      onSignOut();
                    },
                  ),
                  Divider(),
                ],
              ),
            ),
            body: RefreshIndicator(
              onRefresh: () {
                return onRefresh();
              },
              child: ((listNotification.length == 0))
                  ? isLoading
                      ? Container(
                          color: Colors.white,
                          child: Center(
                            child: Loading(
                                indicator: BallPulseIndicator(),
                                size: 50.0,
                                color: Color(0xff3c4b64)),
                          ))
                      : Center(
                child: Container(
                  width: 250,
                  height: 250,

                  decoration: new BoxDecoration(
                    color: Colors.blue.withOpacity(0.1),
                    shape: BoxShape.circle,
                  ),
                  child: InkWell(
                      borderRadius: BorderRadius.circular(250),
                      onTap: (){
                        onRefresh();
                      },
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                            Text("No Notification", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),)
                        ],
                      ),
                    ),
                ),
              )

                  : Container(
                      color: Color(0xffeff0f1),
                      child: IncrementallyLoadingListView(
                        itemCount: () => listNotification.length,
                        hasMore: () => !pageEnd,
                        loadMore: () => nextPage(),
                        loadMoreOffsetFromBottom: 6,
                        itemBuilder: (context, index) {
                          formatText(
                              listNotification[index]["data"]["description"]);
                          return Column(
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  onEnterPWA();
                                },
                                child: Card(
                                  color: Colors.white,
                                  margin: EdgeInsets.symmetric(
                                      vertical: 0, horizontal: 8),
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 8),
                                    child: Container(
                                      width: double.infinity,
                                      height: 80,
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16, vertical: 4),
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              child: CircleAvatar(
                                                radius: 30.0,
                                                backgroundImage: NetworkImage(
                                                    "${Constants.API_URL}/storage/${listNotification[index]["notifiable"]["avatar"]}"),
                                              ),
                                            ),
                                            Expanded(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Flexible(
                                                    child: Container(
                                                      margin: EdgeInsets.only(
                                                          left: 16),
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Container(
                                                            child: Text(
                                                              listNotification[
                                                                          index]
                                                                      ["data"]
                                                                  ["title"],
                                                              style: TextStyle(
                                                                  fontSize: 14,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                          ),
                                                          Container(
                                                              margin: EdgeInsets
                                                                  .only(top: 4),
                                                              child: RichText(
                                                                text: TextSpan(
                                                                  children: formatText(
                                                                      listNotification[index]
                                                                              [
                                                                              "data"]
                                                                          [
                                                                          "description"]),
                                                                ),
                                                              )),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                    width: 70,
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .end,
                                                      children: <Widget>[
                                                        Container(
                                                          child: Text(""),
                                                        ),
                                                        index == -1
                                                            ? Container(
                                                                child: Icon(
                                                                  Icons
                                                                      .fiber_manual_record,
                                                                  color: Colors
                                                                      .blue,
                                                                  size: 18,
                                                                ),
                                                              )
                                                            : Text(""),
                                                        Container(
                                                          child: Text(
                                                            getDuration(listNotification[
                                                                        index][
                                                                    "created_at"]) +
                                                                " ago",
                                                            style: TextStyle(
                                                                fontSize: 11,
                                                                color: Colors
                                                                    .black54),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Divider()
                            ],
                          );
                        },
                      )),
            )),
      ),
    );
  }
}
