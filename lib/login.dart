import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'notification.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:loading/loading.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'constants.dart' as Constants;
class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
 // Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  ValueNotifier<GraphQLClient> client;
  GraphQLClient clientToQuery;
  HttpLink httpLink;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    httpLink = HttpLink(
      uri: '${Constants.API_URL}/graphql',
    );

    client = ValueNotifier(
      GraphQLClient(
        cache: InMemoryCache(),
        link: httpLink,
      ),
    );

    clientToQuery = GraphQLClient(
      cache: InMemoryCache(),
      link: httpLink,
    );
    
    checkLogin();
  }
  
  String Username = "";
  String Password = "";
  String Query = r"""
  mutation 
Login($username:String!, $password:String!)
{
  login(input: {
    username: $username,
    password: $password
  }) {
    access_token
    refresh_token
    user{
      id
      full_name,
      email
      avatar,
    }
    expires_in
    token_type
  }
}
  """;

  String mutationQuery = r"""
  	mutation CreateUserToken($token:String!, $user_id: ID!){
	  createUserToken(input:{
	    user_id: $user_id,
	    platform: "ANDROID",
	    token: $token
	  }){
	  id
	  }
	}
  """;

  Future<Map> fireBaseCloudMessagingListeners(userInfo){

    final AuthLink authLink = AuthLink(
      getToken: () => 'Bearer ${userInfo["access_token"]}',
    );
    final Link link = authLink.concat(httpLink);
    clientToQuery = GraphQLClient(
      cache: InMemoryCache(),
      link: link,
    );
    return _firebaseMessaging.getToken().then((token) async{
      QueryResult mutation = await clientToQuery.mutate(
          MutationOptions(
              document:mutationQuery,
              variables: {
                "user_id": userInfo["user"]["id"],
                "token": token
              }
          ));
     if(mutation.hasException)
       return {"status":false};
     return {"status":true, "id":mutation.data["createUserToken"]["id"]};
    });
    
  }

  final _formKey = GlobalKey<FormState>();
  final myController = TextEditingController();
  var loginLoading = false;
//  @override
//  void dispose() {
//    // Clean up the controller when the widget is removed from the
//    // widget tree.
//    myController.dispose();
//    super.dispose();
//  }

  setLogin(loginInfo) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var user = {
      "access_token": loginInfo["login"]["access_token"].toString(),
      "user": loginInfo["login"]["user"],
      "refresh_token": loginInfo["login"]["refresh_token"].toString()
    };
    fireBaseCloudMessagingListeners(user).then((value){
      user["app_token_id"] = value["id"];

      prefs.setString("longin_info", json.encode(user)).then((value){

        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => NotificationScreen(user:user)),
        );
      });
    });


  }
  checkLogin() async{
    print("check login");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String loginInfo = prefs.getString("longin_info");
    print(loginInfo);
  if(loginInfo!=null){
    final user = json.decode(loginInfo);
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NotificationScreen(user:user)),
    );
  }else{
    setState(() {
      loginLoading = false;
    });
 }
  }
  @override
  Widget build(BuildContext context) {

    return GraphQLProvider(
      client: client,
      child: Scaffold(
        body:
        loginLoading?
        Container(
        color: Colors.white,
        child: Center(
        child: Loading(indicator: BallPulseIndicator(), size: 50.0,color: Color(0xff3c4b64)),
    )):
        Mutation(
          options: MutationOptions(
            document: Query,
            onCompleted: (dynamic resultData) {
              print(resultData);
              if(resultData != null){
                setLogin(resultData);

              }
              setState(() {
                loginLoading = false;
              });
            },
          ),

          builder: (RunMutation login, QueryResult result){

            return Container(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(vertical: 15),
                  child: Text("ERM SYSTEM", style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold
                  , color: Color(0xff3c4b64))),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 8),
                  width: double.infinity,


//              decoration: BoxDecoration(
//                color: Color(0x03c4b64),
//              ),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      children: <Widget>[
                    Container(

                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        onChanged: (text) {
                          setState(() {
                            Username = text;
                          });
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return '*required';
                          }
                          return null;
                        },
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Username'
                      ),
                  ),
                      margin: EdgeInsets.symmetric(vertical: 10),

                    ),
                        Container(
                          child: TextFormField(
                            obscureText: true,
                            onChanged: (text) {
                              setState(() {
                                Password = text;
                              });
                            },
                            validator: (value) {
                          if (value.isEmpty) {
                          return '*required';
                          }else if(value.length<8){
                            return '*minimum 8 digit';
                          }
                          return null;
                          },
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                                //labelText: 'Password'
                              hintText: "Password"
                            ),
                          ),
                          margin: EdgeInsets.symmetric(vertical: 10),
                        ),
                        Container(
                          width: double.infinity,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(4.0),
                                side: BorderSide(color: Color(0xff3c4b64))
                            ),
                            onPressed: (){

                              if(_formKey.currentState.validate()){
                                setState(() {
                                  loginLoading = true;
                                });
                                login(
                                    {
                                      "username":Username, "password":Password
                                    }
                                );

                              }
                            },
                            color: Color(0xff3c4b64).withOpacity(0.8),
                            child: Text("Login", style: TextStyle(color: Colors.white),),
                            padding: EdgeInsets.symmetric(vertical: 16),
                          ),
                          margin: EdgeInsets.symmetric(vertical: 16),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
            );

          },



        ),

      ),
    );
  }
}
