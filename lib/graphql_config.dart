import "package:graphql_flutter/graphql_flutter.dart";
import 'package:flutter/material.dart';
import 'constants.dart' as Constants;
class GraphQLConfiguration {
  static Link link;

  GraphQLConfiguration(String token){
    HttpLink httpLink = HttpLink(
      uri: "${Constants.API_URL}/graphql",
    );

    final AuthLink authLink = AuthLink(
      getToken: ()=> 'Bearer ${token}',
      // OR
      // getToken: () => 'Bearer <YOUR_PERSONAL_ACCESS_TOKEN>',
    );
    link = authLink.concat(httpLink);
  }


  ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClient(
      link: link,
      cache: InMemoryCache(),
    ),
  );

  GraphQLClient clientToQuery() {

    return GraphQLClient(
      cache: InMemoryCache(),
      link: link,
    );
  }
}